/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.Secuencia;


/**
 *
 * @author coloque acá sus nombres completos
 */
public class TestSecuencia {
    
    public static void main(String[] args) {
        Integer v1[]={1,2,3,8,9,10};
	Integer v2[]={4,5,6,7};
		
	Secuencia<Integer> S1 = new Secuencia(v1);
	Secuencia<Integer> S2 = new Secuencia(v2);
        System.out.println("SEC 1: " + S1.toString());
        S1.concat(S2, 4);
        System.out.println("Secuencia S1 unida con S2: " + S1.toString());
        

    }
}