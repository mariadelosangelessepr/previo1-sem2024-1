/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ListaS;

/**
 *
 * @author María de los Ángeles Sepúlveda Prieto
 */
public class TestLista {
    
    public static void main(String[] args) {
        ListaS<Integer> l1=new ListaS();
		l1.insertarFin(1);
		l1.insertarFin(2);
		l1.insertarFin(1);
		l1.insertarFin(3);
		l1.insertarFin(4);
		l1.insertarFin(5);
        System.out.println("Lista: " + l1.toString());
        l1.deleteError();
        System.out.println("Lista corregida: " + l1.toString());

        ListaS<Integer> l2=new ListaS();
		l2.insertarFin(1);
		l2.insertarFin(5);
		l2.insertarFin(2);
		l2.insertarFin(3);
		l2.insertarFin(6);
		l2.insertarFin(2);	
        System.out.println("Lista: " + l2.toString());
     
        l2.deleteError();
    }
}
